<?php

namespace app\models;

use Yii;
use app\models\Respuesta;

/**
 * This is the model class for table "pregunta".
 *
 * @property int $id
 * @property int $id_cuestionario
 * @property string $pregunta
 * @property string $respuesta_1
 * @property string $respuesta_2
 * @property string $respuesta_3
 * @property string $respuesta_4
 * @property string $respuesta_5 indica cual es la respuesta correcta
 * @property string $respuesta_correcta pregunta_1 ,  pregunta_2
 *
 * @property Cuestionario $cuestionario
 * @property Respuesta[] $respuestas
 */
class Pregunta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pregunta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cuestionario', 'pregunta', 'respuesta_1', 'respuesta_2', 'respuesta_correcta'], 'required'],
            [['id_cuestionario'], 'integer'],
            [['pregunta', 'respuesta_1', 'respuesta_2', 'respuesta_3', 'respuesta_4', 'respuesta_5'], 'string'],
            [['respuesta_correcta'], 'string', 'max' => 40],
            [['id_cuestionario'], 'exist', 'skipOnError' => true, 'targetClass' => Cuestionario::className(), 'targetAttribute' => ['id_cuestionario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_cuestionario' => 'Id Cuestionario',
            'pregunta' => 'Pregunta',
            'respuesta_1' => 'Respuesta 1',
            'respuesta_2' => 'Respuesta 2',
            'respuesta_3' => 'Respuesta 3',
            'respuesta_4' => 'Respuesta 4',
            'respuesta_5' => 'Respuesta 5',
            'respuesta_correcta' => 'Respuesta Correcta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuestionario()
    {
        return $this->hasOne(Cuestionario::className(), ['id' => 'id_cuestionario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRespuestas()
    {
        return $this->hasMany(Respuesta::className(), ['id_pregunta' => 'id']);
    }

    public function respuestasCorrectas(){
        $respuestas_correctas = Respuesta::find()->where(['id_cuestionario' => $this->id_cuestionario])->andWhere(['id_pregunta' => $this->id])->andWhere(['respuesta' => $this->respuesta_correcta])->all();
        return count($respuestas_correctas);
    }

    public static function find()
    {
        return new PreguntaQuery(get_called_class());
    }
}
