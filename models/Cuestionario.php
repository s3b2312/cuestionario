<?php

namespace app\models;

use Yii;
use app\models\Respuesta;
use app\models\Pregunta;

/**
 * This is the model class for table "cuestionario".
 *
 * @property int $id
 * @property string $nombre
 * @property string $created_at
 * @property string $estado
 * @property string $contraseña
 *
 * @property Pregunta[] $preguntas
 * @property Respuesta $respuesta
 */

//https://github.com/yiisoft/yii2/issues/14629  la � causa problemas, se cambia el vendor
class Cuestionario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cuestionario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'estado', 'contraseña'], 'required'],
            //[['created_at'], 'safe'],
            [['nombre', 'contraseña'], 'string', 'max' => 255],
            [['estado'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            //'created_at' => 'Created At',
            'estado' => 'Estado',
            'contraseña' => 'Contrase�a',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreguntas()
    {
        return $this->hasMany(Pregunta::className(), ['id_cuestionario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRespuesta()
    {
        return $this->hasOne(Respuesta::className(), ['id_cuestionario' => 'id']);
    }

    public function totalRespuestas(){
        $pregunta =Pregunta::find()->where(['id_cuestionario' => $this->id])->one();
        if($pregunta){
            $id_pregunta = $pregunta->id;
            $dato = Respuesta::find()->where(['id_cuestionario' => $this->id])->andWhere(['id_pregunta' => $id_pregunta])->all();
        }else{
            $dato = [];
        }

        
        return count($dato);
    }
    /**
     * {@inheritdoc}
     * @return CuestionarioQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CuestionarioQuery(get_called_class());
    }
}
