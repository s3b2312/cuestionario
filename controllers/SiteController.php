<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Cuestionario;
use yii\helpers\URL;

class SiteController extends Controller
{
    
    public $layout='usuario';
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Cuestionario::find()->where(['estado' => 'activo']),
        ]);
        return $this->render('index', [
            'cuestionarios' => $dataProvider->getModels()
        ]);
    }

    public function actionProcesar(){
        $nickMaster = "limonay";
        $passMaster = "1";

        $datos = Yii::$app->request->queryParams;
        
        
        if($datos['nick'] == $nickMaster && $datos['pass'] == $passMaster){
            return $this->redirect('/panel/index');
        }else{
            return $this->redirect([
                '/cuestionario/test', 
                'id_cuestionario' => $datos['id_cuestionario'] , 
                'nick' => $datos['nick'] ,
                'pass' => $datos['pass']
            ]);
        }
    }


}
