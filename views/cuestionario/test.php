<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cuestionario */

$this->title = $cuestionario->nombre;
?>
<div class="cuestionario-view">

    <h1><?= "Bienvenido ".$nick ?></h1>
    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $cuestionario,
        'attributes' => [
            'id',
            'nombre',
        ],
    ]) ?>

<form action="/respuesta/myrespuesta">
    <?php foreach ($cuestionario->preguntas as $numero => $pregunta) { ?>
        <div>
            <h3><?= $numero + 1 ?>).- <?= $pregunta->pregunta ?> </h3>

            <div style="margin-left: 20px; margin-bottom: 60px">
                <?php  
                    if($pregunta->respuesta_1){
                        echo "<h3> <input type='radio' value='respuesta_1' name='$pregunta->id' checked>) $pregunta->respuesta_1</h3>";
                    }
                    if($pregunta->respuesta_2){
                        echo "<h3> <input type='radio' value='respuesta_2' name='$pregunta->id'>) $pregunta->respuesta_2</h3>";
                    }
                    if($pregunta->respuesta_3){
                        echo "<h3> <input type='radio' value='respuesta_3' name='$pregunta->id'>) $pregunta->respuesta_3</h3>";
                    }
                    if($pregunta->respuesta_4){
                        echo "<h3> <input type='radio' value='respuesta_4' name='$pregunta->id'>) $pregunta->respuesta_4</h3>";
                    }
                    if($pregunta->respuesta_5){
                        echo "<h3> <input type='radio' value='respuesta_5' name='$pregunta->id'>) $pregunta->respuesta_5</h3>";
                    }

                ?>
            </div>
        </div>
    <?php } ?> 
 
    <input type="hidden" name="nick" value=<?= $nick ?>> 
    <input type="hidden" name="id_cuestionario" value=<?= $id_cuestionario ?>> 
    <input type="submit" value="Enviar" class="btn btn-success">
  </form>




</div>
