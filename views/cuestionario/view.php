<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cuestionario */

$this->title = $model->nombre;
?>


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>

<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>



<style>

    body{
        position: relative;
        background: url('../img/fondo.jpg');
    }

    .cuestionario-view{
        background: rgba(255,255,255,0.9);
        border-radius: 20px;
        padding: 20px;
    }


    .contenedor-respuesta{
     
    }

    .outer {
        width: 300px;
        height: 200px;
        margin: 0 auto;
    }
    .container-chart {
        width: 300px;
        height: 200px;
        float: right;
    }
    .highcharts-yaxis-grid .highcharts-grid-line {
        display: none;
    }

    @media (max-width: 600px) {
        .outer {
            width: 100%;
            height: 200px;
        }
        .container-chart {
            width: 300px;
            float: none;
            margin: 0 auto;
        }

    }

    .correcta{
        background: #61F971;
        border-radius: 10px;
    }

    .contenedor-respuesta h3{
        padding: 4px 10px;
    }

    #container-speed{
    }

</style>



<div class="cuestionario-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="contenedor-respuesta">

    <?php $total_respuestas_pregunta =  $model->totalRespuestas()?>
    <?php foreach ($model->preguntas as $numero => $pregunta) {  ?>

        <div class="outer">
            <div id="<?= $pregunta->id ?>" data-numero="<?= $numero ?>" data-max="<?= $total_respuestas_pregunta ?>" data-valor="<?= $pregunta->respuestasCorrectas() ?>" class="container-chart"></div>
        </div>

        <div>
            <h3><?= $numero + 1 ?>).- <?= $pregunta->pregunta ?> </h3>

            <div style="margin-left: 20px; margin-bottom: 60px">
                <?php  
                    if($pregunta->respuesta_1){
                        if($pregunta->respuesta_correcta == 'respuesta_1'){
                            echo "<h3 class='correcta'>a) $pregunta->respuesta_1</h3>";
                        }else{
                            echo "<h3>a) $pregunta->respuesta_1</h3>";
                        }
                    }
                    if($pregunta->respuesta_2){
                        if($pregunta->respuesta_correcta == 'respuesta_2'){
                            echo "<h3 class='correcta'>b) $pregunta->respuesta_2</h3>";
                        }else{
                            echo "<h3>b) $pregunta->respuesta_2</h3>";
                        }
                    }
                    if($pregunta->respuesta_3){
                        if($pregunta->respuesta_correcta == 'respuesta_3'){
                            echo "<h3 class='correcta'>c) $pregunta->respuesta_3</h3>";
                        }else{
                            echo "<h3>c) $pregunta->respuesta_3</h3>";
                        }
                    }
                    if($pregunta->respuesta_4){
                        if($pregunta->respuesta_correcta == 'respuesta_4'){
                            echo "<h3 class='correcta'>d) $pregunta->respuesta_4</h3>";
                        }else{
                            echo "<h3>d) $pregunta->respuesta_4</h3>";
                        }
                    }
                    if($pregunta->respuesta_5){
                        if($pregunta->respuesta_correcta == 'respuesta_5'){
                            echo "<h3 class='correcta'>e) $pregunta->respuesta_5</h3>";
                        }else{
                            echo "<h3>e) $pregunta->respuesta_5</h3>";
                        }
                    }

                ?>
            </div>
        </div>
    <?php } ?> 
  </div>


  <h1>Particimantes</h1>

</div>


<script>
    var   A = {
        init: function(){
            var graficos = document.querySelectorAll(".container-chart");

            for(var i = 0 ; i < graficos.length ; i++){
                this.dibujar(graficos[i].id,parseInt(graficos[i].dataset.max),parseInt(graficos[i].dataset.valor), "Pregunta " + (parseInt(graficos[i].dataset.numero) + 1));
            }
        },
        dibujar: function(id,max,data,numero_pregunta){    
            var gaugeOptions = {

                chart: {
                    type: 'solidgauge'
                },

                title: null,

                pane: {
                    center: ['50%', '85%'],
                    size: '140%',
                    startAngle: -90,
                    endAngle: 90,
                    background: {
                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                        innerRadius: '60%',
                        outerRadius: '100%',
                        shape: 'arc'
                    }
                },

                tooltip: {
                    enabled: false
                },

                // the value axis
                yAxis: {
                    stops: [
                        [0.1, '#DF5353'], // green
                        [0.5, '#DDDF0D'], // yellow
                        [0.9, '#55BF3B'] // red55BF3B
                    ],
                    lineWidth: 0,
                    minorTickInterval: null,
                    tickAmount: 2,
                    title: {
                        y: -70
                    },
                    labels: {
                        y: 16
                    }
                },

                plotOptions: {
                    solidgauge: {
                        dataLabels: {
                            y: 5,
                            borderWidth: 0,
                            useHTML: true
                        }
                    }
                }
            };

            var chartSpeed = Highcharts.chart(id, Highcharts.merge(gaugeOptions, {
                yAxis: {
                    min: 0,
                    max: max,
                    title: {
                        text: numero_pregunta + "-" + Math.round(((data*100)/max) * 100) / 100 + "%"
                    }
                },

                credits: {
                    enabled: false
                },

                series: [{
                    name: numero_pregunta,
                    data: [data],
                    dataLabels: {
                        format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                            ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/></div>'
                    }
                }]

            }));
        }
    }

    A.init();


   
    // Bring life to the dials
    /*setInterval(function () {
        var point,
            newVal,
            inc;

        if (chartSpeed) {
            point = chartSpeed.series[0].points[0];
            inc = Math.round((Math.random() - 0.5) * 100);
            newVal = point.y + inc;

            if (newVal < 0 || newVal > 200) {
                newVal = point.y - inc;
            }

            point.update(newVal);
        }

    }, 2000);*/

</script>

