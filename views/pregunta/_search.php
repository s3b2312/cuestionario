<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PreguntaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pregunta-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_cuestionario') ?>

    <?= $form->field($model, 'pregunta') ?>

    <?= $form->field($model, 'respuesta_1') ?>

    <?= $form->field($model, 'respuesta_2') ?>

    <?php // echo $form->field($model, 'respuesta_3') ?>

    <?php // echo $form->field($model, 'respuesta_4') ?>

    <?php // echo $form->field($model, 'respuesta_5') ?>

    <?php // echo $form->field($model, 'respuesta_correcta') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
