<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pregunta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pregunta-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_cuestionario')->dropDownList($cuestionarios) ?>

    <?= $form->field($model, 'pregunta')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'respuesta_1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'respuesta_2')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'respuesta_3')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'respuesta_4')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'respuesta_5')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'respuesta_correcta')->dropDownList(['respuesta_1' => 'Respuesta 1', 'respuesta_2' => 'Respuesta 2','respuesta_3' => 'Respuesta 3','respuesta_4' => 'Respuesta 4','respuesta_5' => 'Respuesta 5',]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
